---
title: "STAN"
author: "Conento Decision Science"
date: "2 de julio de 2018"
output:
  html_document:
    theme: cosmo
    toc: true
    toc_depth: 4
    number_sections: true
---

```{r setup, include = FALSE}
knitr::opts_chunk$set(echo = TRUE, message=FALSE, warning=FALSE, fig.align='center')
knitr::opts_knit$set(root.dir = './')

library(tidyverse)

Sys.setenv(BINPREF = "C:/Rtools/mingw_$(WIN)/bin/")
```


Stan code:

```
data {
  real Y[10]; // heights for 10 people
}

parameters {
  real mu; // mean height in population
  real <lower = 0> sigma; // sd of height distribution
}

model {
  for (i in 1:10) {
    Y[i] ~ normal(mu, sigma); // likelihood
  }
  
  mu ~ normal(1.5, 0.1); // Prior for mu
  sigma ~ gamma(1, 1);   // Prior for sigma
}
```

```{r}
library(rstan)

options(mc.cores = parallel::detectCores())
rstan_options(auto_write = TRUE)

# Fake height data
Y <-  rnorm(n = 10, mean = 1.5, sd = 0.2)

# Compile and run MCMC on the Stan program
fit <- stan("simple.stan", iter = 200, chains = 4, data = list(Y = Y))

print(fit, probs = c(0.25, 0.50, 0.75))
```


```{r}
# Extract the posterior samples and plot them
library(ggplot2)

mu <- extract(fit, "mu")[[1]]
qplot(mu)
```

```{r, eval = FALSE}
### Shinystan
library(shinystan)

aFit <- as.shinystan(fit)

launch_shinystan(aFit)
```

A linear model

Stan code:
```
functions {
  real covariateMean(real aX, real aBeta) {
    return(aBeta * log(aX));
  }
  
}

data {
  int N;     // number of people in sample
  real Y[N]; // heights for N people
  real X[N]; // weights for N people
}

parameters {
  real beta; 
  real <lower = 0> sigma; // sd of height distribution
}

model {
  for (i in 1:N) {
    Y[i] ~ normal(covariateMean(X[i], beta), sigma); // likelihood
  }
  
  beta ~ normal(0, 1); // Prior for beta
  sigma ~ gamma(1, 1);   // Prior for sigma
}
```


```{r}
### Linear model
library(rstan)

options(mc.cores = parallel::detectCores())
rstan_options(auto_write = TRUE)

N <- 100
X <- rnorm(N, 60, 10)
beta <- 0.3
sigma <- 0.3

Y <- beta * log(X) + rnorm(N, 0, sigma)

# Compile and run MCMC on the Stan program
fit_2 <- stan("simple_2.stan", iter = 200, chains = 4, 
              data = list(Y = Y, X = X, N = N))

print(fit_2, probs = c(0.25, 0.50, 0.75))

# Extract the posterior samples and plot them

beta <- extract(fit_2, "beta")[[1]]
qplot(beta)
```
Predictions:
```{r}
colMeans(extract(fit_2, "y_hat")[[1]])
```


An example with cross validation

```
data {
  int NTest;
  int NTrain;
  real XTrain[NTrain];
  real XTest[NTest];
}

parameters {
  real nu;
  real mu; // mean height in population
  real <lower = 0> sigma; // sd of height distribution
}

model {
  XTrain ~ student_t(nu, mu, sigma);
  
  nu ~ lognormal(0, 1);
  mu ~ normal(0, 1);
  sigma ~ lognormal(0, 1);
}

generated quantities {
  vector[NTest] logLikelihood;
  
  for(i in 1:NTest) {
    logLikelihood[i] = student_t_lpdf(XTest[i] | nu, mu, sigma);
  }
}

```

```
data {
  int NTest;
  int NTrain;
  real XTrain[NTrain];
  real XTest[NTest];
}

parameters {
  real mu; // mean height in population
  real <lower = 0> sigma; // sd of height distribution
}

model {
  XTrain ~ normal(mu, sigma);
  
  mu ~ normal(0, 1);
  sigma ~ lognormal(0, 1);
}

generated quantities {
  vector[NTest] logLikelihood;
  
  for(i in 1:NTest) {
    logLikelihood[i] = normal_lpdf(XTest[i] | mu, sigma);
  }
}
```

```{r}
### CV
options(mc.cores = parallel::detectCores())
rstan_options(auto_write = TRUE)

N <- 10000
X <- rt(N, 5)

library(caret)

testIdx <- createFolds(X, k = 5, list = TRUE, returnTrain = FALSE)

library(loo)
kFold <- function(aModel, testIdx, X) {
  
  numFolds <- length(testIdx)
  
  # Calculate expected log pairwise predictive density
  lPointLoglikelihoodTotal <- vector()
  
  for(i in 1:numFolds) {
    XTest <- X[testIdx[[i]]]
    XTrain <- X[-testIdx[[i]]]
    
    fit <- sampling(aModel, iter = 200, chains = 4, 
                    data = list(NTest = 2000, NTrain = 8000, 
                                XTrain = XTrain, XTest = XTest))
    logLikelihood1 <- extract_log_lik(fit, "logLikelihood")
    
    lPointLoglikelihood1 <- colMeans(logLikelihood1)
    lPointLoglikelihoodTotal <- c(lPointLoglikelihoodTotal, 
                                  lPointLoglikelihood1)
  }
  return(lPointLoglikelihoodTotal)
  
}

model1 <- stan_model("normal_cv_3.stan")
model2 <- stan_model("student_cv_3.stan")

lELPD1 <- kFold(model1, testIdx, X)
lELPD2 <- kFold(model2, testIdx, X)

sum(lELPD1)

sum(lELPD2)

difference <- sum(lELPD2) - sum(lELPD1)
sd <- sqrt(1000) * sd(lELPD2 - lELPD1)
pvalue <- 1 - pnorm(difference / sd)
pvalue

```



// Simple hierarchichal model

functions {
  
}

data {
  int <lower=0> n;  // Number of observations
  int <lower=0> R;  // number of regions
  int <lower=0, upper=R> r[n]; // region
  real X_1[n]; // Regressors 
  real X_2[n];
  real Y[n];  // Outcome
}

transformed data{
  
}

parameters {
  real BETA_1[R];
  real BETA_2[R];
  real <lower=0> sigma[R];
  
  real BETA_1_top;
  real <lower=0> BETA_1_sigma;
  
  real BETA_2_top;
  real <lower=0> BETA_2_sigma;
}

transformed parameters {
}

model {
  // Likelihood
  for(i in 1:n) {
    int a_region;
    a_region = r[i];
    Y[i] ~ normal(X_1[i] * BETA_1[a_region] + X_2[i] * BETA_2[a_region], 
                  sigma[a_region]);
  }
  
  
  // Priors 
  BETA_1 ~ normal(BETA_1_top, BETA_1_sigma); 
  BETA_2 ~ normal(BETA_2_top, BETA_2_sigma); 
  sigma ~ cauchy(0, 2.5);   
  
  // Hyper-priors
  BETA_1_top ~ normal(0, 1);
  BETA_1_sigma ~ cauchy(0, 2.5);
  BETA_2_top ~ normal(0, 1);
  BETA_2_sigma ~ cauchy(0, 2.5);
}

generated quantities {
  real BETA_1_overall;
  real BETA_2_overall;
  real Y_hat[n];
  vector[n] loglikelihood;
  
  BETA_1_overall = normal_rng(BETA_1_top, BETA_1_sigma);
  BETA_2_overall = normal_rng(BETA_2_top, BETA_2_sigma);
    
  for(i in 1:n) {
    int a_region;
    a_region = r[i];
    
    // Training set fitted values
    Y_hat[i] = normal_rng(X_1[i] * BETA_1[a_region] + X_2[i] * BETA_2[a_region],
                          sigma[a_region]);
                          
    // Training set WAIC and LOO-CV
    loglikelihood[i] = normal_lpdf(Y[i]|BETA_1[a_region]+X_2[i]*BETA_2[a_region],
                                        sigma[a_region]);
  }
}


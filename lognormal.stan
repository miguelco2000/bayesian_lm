functions {
  
}

data {
  int <lower=0> n;  // Number of observations
  int <lower=0> p;  // Number of regressors
  matrix[n, p] X; // Regressors matrix
  vector[n] log_Y;  // Outcome
}

transformed data{
  
}

parameters {
  vector[p] BETAS;
  real <lower = 0> sigma;
}

transformed parameters {
}

model {
  // Likelihood
  log_Y ~ normal(X * BETAS, sigma);
  
  // Priors 
  BETAS ~ normal(0, 1); 
  sigma ~ cauchy(0,2.5);   
}

generated quantities {
  vector[n] log_Y_hat;
  vector[n] mu;
  vector[n] Y_hat;
  vector[n] residuals;
  vector[n] log_residuals;
  
  // X * BETAS ???
  
  mu = X * BETAS;
  for (i in 1:n)
     log_Y_hat[i] = normal_rng(mu[i], sigma);
  
  Y_hat = exp(log_Y_hat);
  
  residuals = exp(log_Y) - Y_hat;
  log_residuals = log_Y_hat - log_Y;
}
  

data {
  int NTest;
  int NTrain;
  real XTrain[NTrain];
  real XTest[NTest];
}

parameters {
  real nu;
  real mu; // mean height in population
  real <lower = 0> sigma; // sd of height distribution
}

model {
  XTrain ~ student_t(nu, mu, sigma);
  
  nu ~ lognormal(0, 1);
  mu ~ normal(0, 1);
  sigma ~ lognormal(0, 1);
}

generated quantities {
  vector[NTest] logLikelihood;
  
  for(i in 1:NTest) {
    logLikelihood[i] = student_t_lpdf(XTest[i] | nu, mu, sigma);
  }
}

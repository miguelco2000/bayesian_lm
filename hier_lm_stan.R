library(tidyverse)
Sys.setenv(BINPREF = "C:/Rtools/mingw_$(WIN)/bin/")

# FAKE normal data for 2 different regions
#
# sigma overall parameter
sigma <- 40

# Top normal distribution parameters
beta_1_top_mu <- 3
beta_1_top_sigma <- 0.2
beta_2_top_mu <- -2
beta_2_top_sigma <- 0.5

# Regional distributions parameters
b_A_1 <- rnorm(1, beta_1_top_mu, beta_1_top_sigma )
b_A_2 <- rnorm(1, beta_2_top_mu, beta_2_top_sigma )
b_B_1 <- rnorm(1, beta_1_top_mu, beta_1_top_sigma )
b_B_2 <- rnorm(1, beta_2_top_mu, beta_2_top_sigma )

# Distributions
N <- 1000

set.seed(123)
x_1 <- sample(1:N, 2*N, replace = TRUE)
x_2 <- sample(1:N, 2*N, replace = TRUE)
  
x_A_1 <- x_1[1:N]
x_A_2 <- x_2[1:N]
y_A <- b_A_1 * x_A_1 + b_A_2 * x_A_2 + rnorm(N, sd = sigma)
summary(y_A)
hist(y_A)

x_B_1 <- x_1[(N+1):(2*N)]
x_B_2 <- x_2[(N+1):(2*N)]
y_B <- b_B_1 * x_B_1 + b_B_2 * x_B_2 + rnorm(N, sd = sigma)
summary(y_B)
hist(y_B)

train_data <- tibble(x_1 = c(x_A_1, x_B_1), x_2 = c(x_A_2, x_B_2),
                     y = c(y_A, y_B), 
                     region = factor(c(rep(1, N), rep(2, N))))

M <- model.matrix(y ~ x_1 + x_2 + region, data = train_data)
X <- M[,1:2]

library(rstan)

options(mc.cores = parallel::detectCores())
rstan_options(auto_write = TRUE)

set.seed(123)

t_ini <- proc.time() 

# Compile and run MCMC on the Stan program
fit_hier_lm <- stan("hier_lm.stan", iter = 2000, chains = 8, 
                 data = list(n = nrow(X),
                             R = 2,
                             r = train_data %>% pull(region) %>% as.integer,
                             X_1 = train_data %>% pull(x_1),
                             X_2 = train_data %>% pull(x_2),
                             Y = train_data %>% pull(y)),
                 control = list(adapt_delta = 0.8))

print(proc.time() - t_ini)

print(fit_hier_lm, probs = c(0.25, 0.50, 0.75))

# Extract the posterior samples and plot them

my_pars <- c("BETA_1[1]" , "BETA_2[1]" , 
             "BETA_1[2]",  "BETA_2[2]",
             "sigma[1]", "sigma[2]",
             "BETA_1_top" , "BETA_1_sigma",
             "BETA_1_overall")

main_res <- extract(fit_hier_lm, pars = my_pars)

print(fit_hier_lm, pars = my_pars)
plot(fit_hier_lm, pars = my_pars)

main_res %>% lapply(mean)

qplot(main_res[["BETA_1[1]"]])

#plotting the posterior distribution for the parameters
post_beta <- As.mcmc.list(fit_hier_lm, pars = my_pars)
plot(post_beta)

pairs(fit_hier_lm, pars = my_pars[1:4])

as.data.frame(fit_hier_lm) %>% as.tibble()

# WAIC and LOO-CV ---------------------------------------------------------

plot(train_data %>% pull(y), extract(fit_hier_lm, "Y_hat")[[1]] %>% colMeans,
     xlab = "Obs.", ylab = "Pred.")

train_data %>% pull(y) %>% mean
Metrics::rmse(train_data %>% pull(y), 
              extract(fit_hier_lm, "Y_hat")[[1]] %>% colMeans)

library(loo)

loglikelihood <- extract_log_lik(fit_hier_lm, "loglikelihood")

WAIC <- waic(loglikelihood)
WAIC

LOO <- loo(loglikelihood)
LOO

